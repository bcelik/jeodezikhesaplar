#!usr/bin/python

import sys 
import math 

class Aci:
    
    
    def dds_ondalik(self,derece,dakika,saniye):
        self.derece = derece
        self.dakika = dakika
        self.saniye = saniye
        if float(dakika > 60.0):
            sys.exit('Hatali Dakika Girdisi!')
        elif float(saniye > 60.0):
            sys.exit('Hatali Saniye Girdisi!')
        return float(derece) + float(dakika)/60 + float(saniye)/3600
    
    def ondalik_dds(self,derece): 
        der = int(derece)
        dakikaondalik = (derece - der)*60
        dak = int(dakikaondalik)
        san = (dakikaondalik - dak)*60 
        return der, dak, san

class Elipsoid:
    """
    Temel Odev Cozumleri icin Elipsoidler ve
    Parametreleri
    """
    def __init__(self, sec):
        self.elipsoids = {'GRS80': 0, 'WGS84':1, 'Bessel':2,
                         'Hayford':3, 'Krassovsky':4}
        
        #elipsoid parametreleri: buyuk eksen, kucuk eksen, basiklik
        self.eparam = [(6378137.000, 6356752.314, 298.2572221),
                       (6378137.000, 6356752.314, 298.2572236),
                       (6377397.155, 6356078.963, 299.1528128),
                       (6378388.000, 6356911.946, 297.0000000),
                       (6378245.000, 6356863.019, 298.3000004)]
        if sec not in self.elipsoids:
            sys.exit("Secilen Elipsoid Bulunamadi")
        else:
            print 'Secilen Elipsoid =', sec 
            self.a = self.eparam[self.elipsoids[sec]][0]
            self.b = self.eparam[self.elipsoids[sec]][1]
            self.f = 1 / self.eparam[self.elipsoids[sec]][2]
            
class TemelOdev1:    
    
    def legendre(self, elp, enl, boyl, s, az):
        
        """
        Legendre Serileri ile Elipsoid Uzerinde 1. Temel
        Odev Cozumu
        """
        
        #Secilen elipsoid icin 1. ve 2. dismerkezlik degerlerinin hesabi
        ekare = (elp.a**2 - elp.b**2)/elp.a**2 
        eskare = (elp.a**2 - elp.b**2)/elp.b**2
        print "Birinci dis merkezlik = ", ekare
        print "Ikinci dis merkezlik = ", eskare
        eta = math.sqrt(eskare * (math.cos(math.radians(enl))**2)) 
        V = math.sqrt(1 + eta**2) 
        c = (elp.a**2)/(elp.b) 
        u = s * math.cos(math.radians(az))
        v = s * math.sin(math.radians(az))
        t = math.tan(math.radians(enl))
        ro = (180/math.pi) 
        #Katsayilar (Enlem)
        b1 = ((V**3)/c)
        b2 = -(
               ((V**4)*t)/(2*(c**2))
               ) 
        b3 = -(
               (3*(V**4)*(eta**2)*t)/(2*(c**2))
               ) 
        b4 = -(
               (V**5)/(6*(c**3))
               )*(1+(3*t**2)+(eta**2)-
                  (9*(eta**2)*t**2)) 
        b5 = -(
               ((V**5)*eta**2)/(2*(c**3))
               )*(
                  1-(t**2)+(eta**2)-(5*(eta**2)*(t**2))
                  )
        #Katsayilar (Boylam)
        l1 = ((V)/(c*(math.cos(math.radians(enl)))))
        l2 = (
              ((V**2)*t)/(
                      (math.cos(math.radians(enl)))*(c**2)
                      )
              )
        l3 = -(
               ((V**3)*(t**2))/(
                       (math.cos(math.radians(enl)))*
                       (3*(c**3))
                       )
               )
        l4 = (
              (V**3)/(
                      3*(c**3)*(math.cos(math.radians(enl)))
                      )
              )*(1+(3*(t**2))+(eta**2))
              
        l5 = -((
              ((V**4)*t)/
              (
               3*(c**4)*(math.cos(math.radians(enl)))
               )
              )*(1+(3*(t**2))))
        #l6 = (((V**4)*t)/(3*(c**4)*math.cos(math.radians(enl))))*(2+(3*(t**2))+eta**2)
        #Katsayilar (Azimut)
        a1 = ((V*t)/c)
        a2 = (
              (V**2)/(2*c**2)
              )*(1+(2*t**2)+eta**2)
        a3 = -(
               ((V**3)*t)/(6*(c**3))
               )*(1+(2*t**2)+eta**2)
        a4 = (
              ((V**3)*t)/(6*(c**3))
              )*(
                 5+(6*(t**2))+(eta**2)-(4*eta**4)
                 )
        print 'Katsayilar (Enlem): b1:%.10e b2:%.10e b3:%.10e b4:%.10e b5:%.10e' % \
        (b1,b2,b3,b4,b5)
        print 'Katsayilar(Boylam): l1:%.10e l2:%.10e l3:%.10e l4:%.10e l5:%.10e' % \
        (l1,l2,l3,l4,l5)
        print 'Katsayilar(Azimut): a1:%.10e a2:%.10e a3:%.10e a4:%.10e' % \
        (a1,a2,a3,a4)
        denlem = (b1*u + b2*(v**2) + b3*(u**2) + b4*u*(v**2) + b5*(u**3))*ro*3600
        dboylam  = (l1*v + l2*u*v + l3*(v**3) + l4*(u**2)*v + l5*u*(v**3))*ro*3600
        dazimut = (a1*v + a2*u*v + a3*(v**3) + a4*(u**2)*v)*ro*3600
        print 'DeltaEnlem = %f ' % denlem
        print 'DeltaBoylam = %f ' % dboylam
        print 'DeltaAzimut = %f ' % dazimut
        en = enl + (denlem/3600)
        boy = boyl + (dboylam/3600)
        azi = az + (dazimut/3600)+180
        return en, boy, azi
    